FROM node
MAINTAINER Fernando Félix y Julian Teran
WORKDIR /app
COPY . .
RUN npm install
EXPOSE ${PORT}
CMD PORT=${PORT} INSTANCE=${INSTANCE} node index.js
